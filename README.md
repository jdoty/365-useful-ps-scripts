This command gets all MFA signin options for all Accounts
you'd need to install the msonline module first
install-module msonline
import-module msonline
connect-msonline
(signin with global admin)

 ```
Get-MsolUser -all | select DisplayName,UserPrincipalName,@{N="MFA Status"; E={ if( $_.StrongAuthenticationMethods.IsDefault -eq $true) {($_.StrongAuthenticationMethods).MethodType} else { "Disabled"}}} |FT -AutoSize
```
This one gets all mailbox rule tenant wide
This one requires the exchangeonline module
install-module exchangeonlinemanagement
import-module exchangeonlinemanagement
connect-exchangeonline

```
$users = (get-mailbox -RecipientTypeDetails userMailbox -ResultSize unlimited ).UserPrincipalName
foreach ($user in $users)
{
$rules=Get-InboxRule -Mailbox $user 
if($rules.length -gt '0') {
$rules | Export-Csv ~/desktop/inboxrules.csv -NoTypeInformation -Append
}

 

}
```


 
And then to check mail delegation
you need exchange online loaded and connected
```
Get-Mailbox | Get-MailboxPermission | where {$_.user.tostring() -ne "NT AUTHORITY\SELF" -and $_.IsInherited -eq $false} |select Identity,User,AccessRights | Export-Csv -NoTypeInformation ~/desktop/mailboxpermissions.csv -Append
```